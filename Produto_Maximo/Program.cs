﻿using System;

namespace Produto_Maximo
{
    class Program
    {
        static void Main(string[] args)
        {
            Operacao op = new Operacao();

            op.RetirarZeros();
            op.VerificarNegativos();
            op.VerificarPositivos();
            op.CasoEspecial();
            op.Produto_Max();
            Console.WriteLine();

        }
    }
}
