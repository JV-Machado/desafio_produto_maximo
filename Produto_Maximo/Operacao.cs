﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace Produto_Maximo
{
    class Operacao
    {
        public List<int> Lista { get; set; }
        public List<int> Negativos { get; set; }
        public List<int> Positivos { get; set; }
        public List<int> Especial { get; set; }
        public int Produto { get; set; }
        public int[] Termos { get; set; }
        public int Cont { get; set; }
        public int Tamanho { get; set; }

        public Operacao()
        {
            Lista = new List<int>() { -6, 4, -5, 8, -10, 0, 8 };
            Negativos = new List<int>();
            Positivos = new List<int>();
            Especial = new List<int>();
            Termos = new int[20];
        }

        public void RetirarZeros()
        {
            if (Lista.Count > 2)
            {
                Lista.RemoveAll(x => x == 0);
            }
            else
            {
                return;
            }
        }

        public void VerificarNegativos()
        {
            if(Lista.Count == 2)
            {
                return;
            }
            foreach (int i in Lista)
            {
                if (i < 0)
                {
                    Cont++;
                    Negativos.Add(i);
                }
            }

            if (Cont % 2 == 1)
            {
                Negativos.Remove(Negativos.Max());
            }

        }
        public void VerificarPositivos()
        {
            if (Lista.Count == 2)
            {
                return;
            }
            foreach (int i in Lista)
            {               
                if (i > 0)
                {
                    Positivos.Add(i);
                }
            }
        }

        public void CasoEspecial()
        {
            if (Lista.Count == 2)
            {
                foreach (int i in Lista)
                {
                    Especial.Add(i);
                }
            }
        }
        public void Produto_Max()
        {
            Produto = 1;
            if(Lista.Count > 1)
            {
                for (int n = 0; n < Negativos.Count; n++)
                {
                    Produto = Produto * Negativos[n];
                    Termos[n] = Negativos[n];
                    Tamanho++;
                }

                for (int p = 0; p < Positivos.Count; p++)
                {
                    Produto = Produto * Positivos[p];
                    Termos[p + Negativos.Count] = Positivos[p];
                    Tamanho++;
                }
                for(int e = 0; e < Especial.Count; e++)
                {
                    Produto = Produto * Especial[e];
                    Termos[e] = Especial[e];
                    Tamanho++;
                }
                Console.WriteLine($"Maior Produto: {Produto}");
                Console.WriteLine();
                Console.Write("Termos: ");
                for (int i = 0; i < Tamanho; i++)
                {
                    Console.Write($"{Termos[i]} ");
                }

            }
            else
            {
                Console.WriteLine("Componentes insuficientes para formar um produto");
            }
        }

    }
}
